test: fibonacci_heap_test
	./bin/$< > result.txt
# for testing on windows, change $< to $<.exe

CXXFLAGS=-std=c++17 -O2 -Wall -Wextra -g -Wno-sign-compare

fibonacci_heap_test: fibonacci_heap/fibonacci_heap.h fibonacci_heap/fibonacci_heap.cpp
	mkdir -p bin
	$(CXX) $(CXXFLAGS) $^ -o bin/$@

clean::
	rm -rf bin

.PHONY: clean test
