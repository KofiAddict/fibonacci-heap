#include <vector>

template <typename T>
struct FibonacciHeap;

/*
    Templated node of a fibonacci heap
    Contains priority, templated payload, pointers at parent, first child and previous and next siblings (in cycle - last points at first)
    Rank marks number of children
*/
template <typename T>
struct Node
{
	T payload;
	int priority() const { return prio; }

	Node& operator =(const Node node)
	{
		if (this != &node)
		{
			parent = node.parent;
			first_child = node.first_child;
			rank = node.rank;
			payload = T(node.payload);

			prev_sibling = node.prev_sibling;
			next_sibling = node.next_sibling;

			prio = node.prio;
			marked = node.prio;
		}

		return *this;
	}

	Node& operator =(Node&& node) noexcept
	{
		if (this != &node)
		{
			parent = exchange(node->parent, nullptr);
			first_child = exchange(node->first_child, nullptr);
			rank = exchange(node->rank, 0);
			payload = move(node->payload);

			prev_sibling = exchange(node->prev_sibling, nullptr);
			next_sibling = exchange(node->next_sibling, nullptr);

			prio = exchange(node->prio, 0);
			marked = exchange(node->marked, false);
		}

		return *this;
	}

private:
	int prio;

	Node<T> *parent = nullptr, *first_child = nullptr;
	Node<T> *prev_sibling, *next_sibling;

	int rank = 0;
	bool marked = false;

	Node(int p, T payload) : payload(payload), prio(p), prev_sibling(this), next_sibling(this)
	{
	}

	Node(Node<T>&& node) noexcept : parent()
	{
		parent = exchange(node->parent, nullptr);
		first_child = exchange(node->first_child, nullptr);
		rank = exchange(node->rank, 0);
		payload = move(node->payload);

		prev_sibling = exchange(node->prev_sibling, nullptr);
		next_sibling = exchange(node->next_sibling, nullptr);

		prio = exchange(node->prio, 0);
		marked = exchange(node->marked, false);
	}

	Node(Node<T> const& node)
	{
		parent = node->parent;
		first_child = node->first_child;
		rank = node->rank;
		payload = T(node->payload);

		prev_sibling = node->prev_sibling;
		next_sibling = node->next_sibling;

		prio = node->prio;
		marked = node->prio;
	}

	friend struct FibonacciHeap<T>;
};

/*
    Templated fibonacci heap
    Roots will be considered as children of meta root.
 */
template <typename T>
struct FibonacciHeap
{
	bool empty() const { return size_ == 0; }

	size_t size() const { return size_; }

	/*
	    Inserts a new node with given priority and payload
	 */
	Node<T>* push(int prio, T payload = T())
	{
		Node<T>* n = new Node<T>(prio, payload);
		add_child(&meta_root, n);
		size_++;
		return n;
	}

	/*
		Constructs and pushes a new element into the heap
	 */
	Node<T>* emplace(int prio, T payload = T())
	{
		Node<T>* n = new Node<T>(prio, T(payload));
		add_child(&meta_root, n);
		size_++;
		return n;
	}

	/*
	    Removes the lowest priority node
	    Returns pair of payload and priority of node with lowest priority
	 */
	std::pair<T, int> pop_min()
	{
		Node<T>* min = meta_root.first_child;
		Node<T>* node = min->next_sibling;
		while (node != meta_root.first_child)
		{
			if (node->prio < min->prio) min = node;
			node = node->next_sibling;
		}

		// Add all its subtrees to the heap.
		while (min->rank > 0)
			add_child(&meta_root, remove(min->first_child));

		// Remove the root.
		std::pair<T, int> ret = {min->payload, min->prio};
		delete remove(min);
		size_--;

		// Finally, consolidate the heap.
		consolidate();

		return ret;
	}

	/*
	    Returns pair of payload and priority of node with lowest priority
	 */
	Node<T>* peek_min()
	{
		Node<T>* min = meta_root.first_child;
		Node<T>* node = min->next_sibling;
		while (node != meta_root.first_child)
		{
			if (node->prio < min->prio) min = node;
			node = node->next_sibling;
		}

		consolidate();

		return min;
	}

	/*
	    Decrease priority of node to new_prio.
	    new_prio must not be higher than node.prio.
	 */
	void decrease(Node<T>* node, int new_prio)
	{
		// set the new priority of node
		node->prio = new_prio;

		// if the node is a root node, we only need to consolidate the heap, since the global minimum could have changed
		if (is_root(node))
			return;

		// if the heap property is not broken, we return immediately
		if (node->parent->prio < node->prio)
			return;

		// if the heap property is broken, we iterate 
		Node<T>* current = node;
		while (true)
		{
			Node<T>* next = current->parent;

			if (is_root(current->parent))
			{
				cut(current);
				break;
			}

			if (!current->parent->marked)
			{
				current->parent->marked = true;
				cut(current);
				break;
			}
			else
				cut(current);
			current = next;
		}
	}

	void swap(FibonacciHeap<T>& heap) noexcept
	{
		size_ = exchange(heap->size_, size_);
		meta_root = exchange(heap->meta_root, meta_root);
	}

	FibonacciHeap() : size_(0), meta_root(int(), T())
	{
	}

	FibonacciHeap(FibonacciHeap<T> const& heap)
	{
		size_ = heap.size_;
		meta_root = *copy_layer(&heap.meta_root, nullptr);
	}

	FibonacciHeap(FibonacciHeap<T>&& heap) noexcept
	{
		size_ = 0;
		meta_root = nullptr;

		heap.swap(*this);
	}

	FibonacciHeap& operator=(const FibonacciHeap& heap)
	{
		if (&this == &heap)
			return *this;

		FibonacciHeap(heap).swap(*this);
		return *this;
	}

	FibonacciHeap& operator=(FibonacciHeap&& heap) noexcept
	{
		if (this != &heap)
		{
			size_ = 0;
			meta_root = nullptr;

			heap.swap(*this);
		}

		return *this;
	}

	~FibonacciHeap()
	{
		if (size_ == 0) return;
		Node<T>* next;
		Node<T>* n = meta_root.first_child;
		do
		{
			while (n->first_child) n = n->first_child;
			next = (n->next_sibling != n) ? n->next_sibling : n->parent;
			delete remove(n);
		}
		while ((n = next) != &meta_root);
	}

protected:
	size_t size_;
	Node<T> meta_root;

	/*
	    Iteratively merges roots of same rank into roots of +1 higher rank
	    until there is at most one root of each rank
	*/
	void consolidate()
	{
		std::vector<Node<T>*> buckets(max_rank() * 2 + 1, nullptr);
		while (meta_root.first_child)
		{
			Node<T>* node = remove(meta_root.first_child);
			while (Node<T>*& b = buckets.at(node->rank))
			{
				node = pair_trees(b, node);
				b = nullptr;
			}
			buckets.at(node->rank) = node;
		}

		for (Node<T>* node : buckets)
			if (node) add_child(&meta_root, node);
	}

	// Join two trees of the same rank.
	Node<T>* pair_trees(Node<T>* a, Node<T>* b)
	{
		if (a->prio > b->prio) std::swap(a, b);
		add_child(a, b);
		return a;
	}

	void cut(Node<T>* node)
	{
		node->marked = false;
		add_child(&meta_root, remove(node));
	}

	// Return maximum possible rank of a tree in a heap of the current size_.
	size_t max_rank()
	{
		size_t ret = 1;
		while (1ull << ret <= size_) ret++;
		return ret;
	}

	// Check whether a node is the root.
	bool is_root(Node<T>* n)
	{
		return n->parent == &meta_root;
	}

	// Link together two elements of linked list -- a and b.
	void join(Node<T>* a, Node<T>* b)
	{
		a->next_sibling = b;
		b->prev_sibling = a;
	}

	// Add node as a child of a given parent.
	void add_child(Node<T>* parent, Node<T>* node)
	{
		if (parent->rank == 0)
			parent->first_child = node;
		else
		{
			join(parent->first_child->prev_sibling, node);
			join(node, parent->first_child);
		}

		node->parent = parent;
		parent->rank++;

		if (parent == &meta_root)
		{
			node->marked = false;
		}
	}

	// Disconnect a node from its parent.
	Node<T>* remove(Node<T>* n)
	{
		n->parent->rank--;
		if (n->parent->first_child == n) n->parent->first_child = n->next_sibling;
		if (n->parent->rank == 0) n->parent->first_child = nullptr;

		n->parent = nullptr;
		join(n->prev_sibling, n->next_sibling);
		join(n, n);

		return n;
	}

private:
	Node<T>* copy_layer(Node<T>& first_sibling, Node<T>& parent)
	{
		if (!first_sibling)
		{
			return nullptr;
		}

		Node<T>* first_copied = &Node<T>(first_sibling);

		if (first_sibling->next_sibling == first_sibling)
		{
			join(first_copied, first_copied);
			first_copied->parent = parent;
			first_copied->first_child = copy_layer(first_copied->first_child);
			return first_copied;
		}

		Node<T>* prev_copied = first_copied;
		Node<T>* current = first_sibling.next_sibling;

		do
		{
			Node<T>* current_copied = &Node<T>(current);
			current_copied->parent = &parent;
			current_copied->first_child = copy_layer(current_copied->first_child, current_copied);
			join(prev_copied, current_copied);
			prev_copied = current_copied;
			current = current->next_sibling;
		}
		while (current != &first_sibling);

		join(prev_copied, first_copied);

		return first_copied;
	}
};
